#!/bin/sh

source .env.sh

# Default requirements
if [ -e "requirements.txt" ]
then
    pip install -r requirements.txt --no-cache-dir
fi

# Requirements exported from the container when run in local
if [ -e "requirements-export.txt" ]
then
    pip install -r requirements-export.txt --no-cache-dir
fi

pip freeze > requirements-export.txt