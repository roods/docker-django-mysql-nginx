#!/bin/sh

source .env.sh

# Waiting for DB
while ! nc -z db 3306 ; do
    echo "Waiting for the MySQL Server"
    sleep 3
done

python /app/manage.py migrate

if [ $ENVIRONMENT == "local" ]
then
   python /app/manage.py runserver 0.0.0.0:8000
else
   python /app/manage.py collectstatic --noinput
   uwsgi --socket :9000 --workers 4 --master --enable-threads --module nomoportfolio.wsgi
fi


