from django.db import models


class AppUser(models.Model):
	name = models.CharField(max_length=50)
	attachement = models.FileField()
	email = models.CharField(max_length=100)
	phone = models.IntegerField()
	address = models.CharField(max_length=250)
	address2 = models.CharField(max_length=250)
	
	class Meta:
		db_table = "appusers"
	
	def __str__(self):
		return self.name