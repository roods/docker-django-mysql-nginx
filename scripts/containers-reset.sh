#!/bin/sh



# This file is to be executed at the root of projects
source .env.sh
# Copy the file to define how the server will be rendered
cp .env.sh app/.env.sh

# Clean all the containers and images to start fresh
docker stop $(docker ps -a -q --filter="name=$PROJECT")
docker-compose -f docker-compose.$ENVIRONMENT.yml down --volumes --rmi all
docker-compose -f docker-compose.$ENVIRONMENT.yml up --build