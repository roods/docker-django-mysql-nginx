#!/bin/sh

source .env.sh


if [ $ENVIRONMENT == "local" ]
then
   docker exec -it $PROJECT_NAME-app pip uninstall $1 --no-cache-dir
   docker exec -it $PROJECT_NAME-app pip freeze > app/requirements-export.txt
else
   echo "plugins can only be installed this way in a local environment"
fi