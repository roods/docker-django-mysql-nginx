#!/bin/sh

source .env.sh


if [ $ENVIRONMENT == "local" ]
then
#   docker-compose -f docker-compose.local.yml run --rm app sh -c "pip install $1 --no-cache-dir && pip freeze > requirements-export.txt"
   docker exec -it $PROJECT_NAME-app pip install $1 --no-cache-dir
   docker exec -it $PROJECT_NAME-app pip freeze > app/requirements-export.txt
else
   echo "plugins can only be installed this way in a local environment"
fi